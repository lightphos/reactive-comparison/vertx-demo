package com.demo.reactive.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;

/**
 * vertxreactive, 26/08/2017.
 */
public class ReactVerticle extends AbstractVerticle {

    static int c; // any problem with this :-)
    @Override
    public void start(Future<Void> fut) {


        Router router = Router.router(vertx);
        router.route("/services").handler(r -> {
            System.out.println(++c + "rx ");
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            r.response()
                .putHeader("content-type", "text/html")
                .end("<h1>Vert.x 3</h1>");
        });


        vertx
            .createHttpServer()
            .requestHandler(router::accept)
            .listen(8080, result -> {
                if (result.succeeded()) {
                    fut.complete();
                } else {
                    fut.fail(result.cause());
                }
            });
    }

}
